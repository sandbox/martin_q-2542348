<?php
/**
 * @file
 * Theme implementation for the Scald HTML5 Audio and Video Player
 * Created by martin_q
 * Based on previous work by SylvainM, cmi75, defr
 *
 * Available variables:
 * - $vars['sources'] : Urls of multiple sources.
 * - $vars['thumbnail'] : Uri of the image.
 * - $vars['type'] : 'scald_html5_audio' or 'scald_html5_video'.
 * - $vars['atom'] : The atom being played.
 * - $vars['class'] : The CSS class (or classes, separated by spaces) to apply.
 * - $vars['thumbnail'] : An absolute URL to a thumbnail image file. (TODO change code in prerender to apply file_create_url.)
 */
$dimensions = 'width="' . $vars['width'] . '"';
if ($vars['type'] == 'scald_html5_video') {
  $dimensions .= ' height="' . $vars['height'] . '"';
  $tag = 'video';
}
else {
  $tag = 'audio';
}
$id  = 'scald-' . $tag . '-' . $vars['atom']->sid;
?>

<?php print '<' . $tag . ' ' . $dimensions; ?> id="<?php print $id; ?>" controls="controls" class="<?php print $vars['class']; ?>" <?php if (!empty($vars['thumbnail'])): ?>poster="<?php print $vars['thumbnail'] . '"'; endif ?>>
  <?php if (isset($vars['source']) && isset($vars['mime_type'])): ?>
  <source src="<?php print $vars['source']; ?>" type="<?php print $vars['mime_type']; ?>" />
  <?php endif; ?>
  <?php foreach ((array) $vars['sources'] as $source): ?>
  <source src="<?php print $source['path']; ?>" type="<?php print $source['mime_type']; ?>" />
  <?php endforeach ?>
<?php print "</" . $tag . '>'; ?>